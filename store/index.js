import Vue from 'vue'
import Vuex from 'vuex'

import $H from '../common/request.js';
import io from '../common/uni-socket.io.js';
import $C from '../common/config.js';

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		user: null,
		token: null,
		socket: null
	},
	actions: {
		// 连接socket
		connectSocket({
			state,
			dispatch
		}) {
			let s = io($C.socketUrl, {
				query: {},
				transports: ['websocket'],
				timeout: 5000 //过时
			})
			let onlineEvent = (e) => {
				// console.log(e);
				uni.$emit('live',{
					type:'online',
					data:e
				})
			}
			let commentEvent = (e) => {
				// console.log(e);
				uni.$emit('live',{
					type:'comment',
					data:e
				})
			}
			let giftEvent=(e) => {
				// console.log(e);
				uni.$emit('live',{
					type:'gift',
					data:e
				})
			}
			// 监听连接
			s.on('connect', () => {
				console.log('已连接');
				state.socket = s
				// socket.io唯一链接id，可以监控这个id实现点对点通讯
				const {
					id
				} = s
				// 后台返回的error
				s.on(id, (e) => {
					// console.log(e);
					if (e.data.action == 'error') {
						if(e.meta.notoast){
							return
						}
						uni.showToast({
							title: e.data.payload,
							icon: 'none'
						});
						if(e.data.payload=='token令牌不合法1'){
							uni.navigateTo({
								url: '../login/login'
							});
						}
					}
				})
				// console.log(id);
				// 发送数据
				// s.emit('joinLive',{data:'123'})
				// 监听进入直播间
				s.on('online', onlineEvent)
				// 监听发送信息
				s.on('comment', commentEvent)
				// 监听发送礼物
				s.on('gift',giftEvent)

			})
			// 移除监听事件
			let removeListener = () => {
				if (s) {
					s.removeListener('online', onlineEvent)
					s.removeListener('comment', commentEvent)
					s.removeListener('gift', giftEvent)
				}
			}


			// 监听失败
			s.on('error', () => {
				removeListener()
				state.socket=null
				console.log('失败');
			})
			// 监听断开
			s.on('disconnect', () => {
				removeListener()
				state.socket=null
				console.log('断开');
			})
		},
		// 登录权限
		authMethod({
			state
		}, callback) {
			if (!state.token) {
				uni.showToast({
					title: '请先登录',
					icon: 'none'
				});
				return uni.navigateTo({
					url: '/pages/login/login'
				});
			}
			callback()
		},
		// 初始化用户登录状态
		initUser({
			state
		}) {
			let u = uni.getStorageSync('user')
			let t = uni.getStorageSync('token')
			if (u) {
				state.user = JSON.parse(u)
				state.token = t
			}
		},
		logout({
			state
		}) {
			$H.post('/logout', {}, {
				token: true,
				toast: false
			})
			state.user = null
			state.token = null
			uni.removeStorageSync('user')
			uni.removeStorageSync('token')
		},
		login({
			state
		}, user) {
			state.user = user
			state.token = user.token

			uni.setStorageSync('user', JSON.stringify(user))
			uni.setStorageSync('token', user.token)
		},
		getUserInfo({
			state
		}) {
			$H.get('/user/info', {
				token: true,
				noJump: true,
				toast: false
			}).then(res => {
				state.user = res
				uni.setStorage({
					key: "user",
					data: JSON.stringify(state.user)
				})
			})
		}
	}
})
